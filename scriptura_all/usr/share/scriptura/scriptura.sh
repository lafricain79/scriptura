#!/bin/bash

while :; do
    notify-send "Scriptura" "$(head -n1 /usr/share/scriptura/versets_fr)" -i /usr/share/scriptura/icon/bible1.png -t 8000   # Afficher dans la notification la première ligne du fichier
    sed -i '1{H;d}; ${p;x;s/^\n//}' /usr/share/scriptura/versets_fr     # Faire passer la première ligne du fichier à la fin du fichier
    sleep 15m                                                            # Attendre 10 minutes avant de recommencer
done



#while :; do ((compteur++)); notify-send "Lire la Bible" "$(sed -n ${compteur}p /usr/share/lirelabible/versets_fr.txt)" -i /usr/share/lirelabible/icon/bible1.png -t 6000; sleep 15m; done
